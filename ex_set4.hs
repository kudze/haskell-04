module ExSet4 where
import Data.List;

--1
data GTree a = Leaf a | Gnode [GTree a]

--return the depth of a GTree;
depth :: GTree a -> Integer
depth (Leaf _) = 0
depth (Gnode x) = foldr (\xs c -> max (1 + (depth xs)) c) 0 x

--find whether an element occurs in a GTree;
inTree :: Eq a => GTree a -> a -> Bool
inTree (Leaf a) t = a == t
inTree (Gnode x) t = foldr (||) False (map (\xs -> inTree xs t) x)

--map a given function over the elements at the leaves of a GTree (i.e., a
--variation of the mapTree function from the Lecture 11 slides for GTree).
mapTree :: (a -> b) -> GTree a -> GTree b
mapTree f (Leaf a)  = Leaf (f a)
mapTree f (Gnode x) = Gnode (map (mapTree f) x)

--2 ???
data Expr a = Lit a | EVar Var | Op (Ops a) [Expr a]
type Ops a = [a] -> a
type Var = Char

--Define a new type
--Valuation a, relating variables (of the type Var) with the values of the type
--a.
type Valuation a = [(Var, a)]


-- for the given variable valuation and expression, evaluates (folds) the
-- expression to a single value.
eval :: Valuation a -> Expr a -> a
eval _ (Lit a) = a

eval [] (EVar a) = error "Evar not in valuation"
eval ((vVar, va):xs) (EVar a)
    | vVar == a = va
    | otherwise = eval xs (EVar a)

eval x (Op op exprs) = op (map (eval x) exprs)

--3
--lecture:
type RegExp = String -> Bool

epsilon :: RegExp
epsilon = (=="")

char :: Char -> RegExp
char ch = (==[ch])

(|||) :: RegExp -> RegExp -> RegExp
e1 ||| e2 = \x -> e1 x || e2 x

(<*>) :: RegExp -> RegExp -> RegExp
e1 <*> e2 = \x -> or [e1 y && e2 z | (y,z) <- splits x]

splits :: String -> [(String,String)] 
splits s = map (\x -> splitAt x s) [0, 1 .. (length s)]

star :: RegExp -> RegExp
star p = epsilon ||| (p ExSet4.<*> star p)

--where option p matches zero or one occurrences of the pattern p, and plus
--p matches one or more occurrences of the pattern p.
option, plus :: RegExp -> RegExp

option reg = reg ||| epsilon

(&&&) :: RegExp -> RegExp -> RegExp
e1 &&& e2 = \x -> e1 x && e2 x

plus reg = \x -> (star reg x) && (not (epsilon x))

--4
data NumList a = Nlist [a]

--Make this type an instance of the type classes Eq and Ord by defining
--that such lists are equal only if their average values are equal, and such lists
--can be compared only by comparing their respective average values. Add
--the needed context (type class dependencies) to make such instances work.
--The average value for [] is 0.

average :: Fractional a => NumList a -> a
average (Nlist []) = 0
average (Nlist x) = sum x / fromIntegral (length x)

instance (Fractional a, Eq a) => Eq (NumList a) where
    (Nlist x) == (Nlist y) = average (Nlist x) == average (Nlist y)

instance (Fractional a, Ord a) => Ord (NumList a) where
    compare (Nlist x) (Nlist y) = compare (average (Nlist x)) (average (Nlist y))

--5
data Result a = OK a | Error String

composeResult :: (a -> Result b) -> (b -> Result c) -> (a -> Result c)
composeResult a b = \x -> case (a x) of
    Error ae -> Error ae
    OK ar -> b ar

-- for testing
getResult :: Result a -> a
getResult (Error e) = error e
getResult (OK a) = a

--6

--from lecture
primes :: [Integer]
primes = sieve [2 ..]

sieve :: [Integer] -> [Integer]
sieve [] = []
sieve (x:xs) = x : sieve [y | y <- xs, y `mod` x > 0]

--that any even number greater than 2 can be rewritten as a sum of two prime numbers.
--that checks that the conjecture is true up to the given upper bound
primesMax :: Integer -> [Integer]
primesMax max 
    | max >= 2 = sieve [2 .. max]
    | otherwise = error "Max should be >= 2"

goldbach :: Integer -> Bool
goldbach max = and [or [x + y == z | (x) <- p, (y) <- p] | (z) <- [4,6..max]]
    where
        p = primesMax max

--7
data Stream a = Cons a (Stream a)

--creates an infinite list out of the given stream;
streamtoList :: Stream a -> [a]
streamtoList (Cons x xs) = x : (streamtoList xs)

--creates a stream for the given iteration function and the starting stream element (seed);
streamIterate :: (a -> a) -> a -> Stream a
streamIterate f seed = Cons seed (streamIterate f (f seed))

--merges two streams into one so that their elements are interleaved. In
--other words, for two given streams he11, e12, e13, ...i and he21, e22, e23, ...i,
--the result would be the stream he11, e21, e12, e22, ...i
streamInterleave :: Stream a -> Stream a -> Stream a
streamInterleave (Cons x xs) (Cons y ys) = Cons x (Cons y (streamInterleave xs ys))

--test no interleave:       take 10 $ streamtoList (streamIterate (+1) 5)
--test interleave:          take 10 $ streamtoList (streamInterleave (streamIterate (+1) 5) (streamIterate (+2) 0))